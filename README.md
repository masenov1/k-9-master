### K-9 Email - Improved color picker ###

*For my Introduction to Software Engineering class, I decided to improve one of the features of my favourite email client for Android - K-9 Email. The differences can be seen below.
*


**Languages/Libraries used:
**

* Java (Android)


**Screenshots:
**

Before and After...

![screenshot2.png](https://bitbucket.org/repo/GABqqk/images/231360366-screenshot2.png)
![screenshot1.png](https://bitbucket.org/repo/GABqqk/images/2560242675-screenshot1.png)

**Video:
**

(Clicking on the video will redirect you to YouTube)

[![Screenshot.png](https://bitbucket.org/repo/GABqqk/images/196860813-Screenshot.png)](http://youtu.be/_-A5KBP5IDo)